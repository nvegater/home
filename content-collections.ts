import { defineCollection, defineConfig } from "@content-collections/core";
import { compileMDX } from "@content-collections/mdx";

import remarkGfm from "remark-gfm";
import rehypeSlug from "rehype-slug";
import rehypeAutolinkHeadings from "rehype-autolink-headings";
import rehypeHighlight from "rehype-highlight";

const posts = defineCollection({
  name: "posts",
  directory: "src/posts",
  include: "*.mdx",
  schema: (z) => ({
    title: z.string().describe("The title of the post"),
    date: z.string().describe("The date of the post"),
    excerpt: z.string().describe("The short description of the post"),
    tags: z.array(z.string()).optional(),
  }),
  transform: async (post, context) => {
    const mdx = await compileMDX(context, post, {
      remarkPlugins: [
        // support for GitHub Flavored Markdown https://github.github.com/gfm/
        remarkGfm,
      ],
      rehypePlugins: [
        // looks for headings
        // (so <h1> through <h6>)
        // without ids and adds id attributes to them
        // based on the text they contain
        rehypeSlug,
        // add links from headings back to themselves
        [rehypeAutolinkHeadings, { behavior: "wrap" }],
        // apply syntax highlighting to code with lowlight.js
        rehypeHighlight,
      ],
    });

    return {
      ...post,
      mdx,
    };
  },
  onSuccess: (docs) => {
    console.log(`generated collection with ${docs.length}`);
  },
});

export default defineConfig({
  collections: [posts],
});
