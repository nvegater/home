# Ideas for Editor

The Editor has 3 Modes.
Maybe a 4 mode is with a Panel like https://jspanel.de/ that shows the editor on top of the preview or vice versa.

```typescript
type EditorMode = "editor" | "preview" | "both";
```

A sticky Navbar that Transforms based on the mode.
It shows tools that are useful for the current Mode.

## Sticky Navbar universal Buttons.

Always Present and not specific to any mode, actions I want to execute regardless of the editing Mode.

* Mode Toggle: Switch that cycles between all the editor Modes.

## Sticky navBar on Editing and Preview Mode

The idea is to have Functions that focus on efficient editing based on how the article looks like.
For instance, if I think the paragraphs looks to long (based on the loyal Preview), 
I can click on the paragraph in the Preview and the Editor will scroll to that paragraph and place the cursor there.
I can also select a text in the Preview, and display a Menu with options. 
The normal ones like copy paste etc but also some custom options -> like navigate to the Editor and highlight the text.

* Button 1: Cursor position in the Editor -> Navigates to that section in the Preview.
On Select in the Preview -> Show a Menu with option to navigate to the Editor.
how? unique identifiers for each element in the Editor State that matches the Preview State.
Button is blurred if cursor is not in the Editor.

## Sticky navBar on only Editing Mode

In this case I dont care about the Preview, I want to focus on the editing.

* Button 3: Focus

When cursor is on Editor, is possible to click ano