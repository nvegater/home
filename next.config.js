/** @type {import('next').NextConfig} */

const { withContentCollections } = require("@content-collections/next");

const nextConfig = {
  reactStrictMode: true,
  rewrites: async () => [
    {
      source: "/public/resume.html",
      destination: "/pages/api/resume.js",
    },
  ],
};

module.exports = withContentCollections(nextConfig);
