const pathNode = require("path");
const fileSystem = require("fs");

// URL of the public gist containing resume data
const PUBLIC_GIST_URL =
  "https://gist.githubusercontent.com/nvegater/52d52638764ad11bb7d093ea4bad491c/raw/6bb584cdae26292bc8208434d1c8425ad3396d2f/resume.json";
// Name of the local file to store resume data
const resumeName = `resume.json`;

/**
 * Updates a local file with the latest data from a remote gist
 * @param {string} path - The directory path where the local file is located
 */
async function updateLocalFileFromGist(path) {
  const resumePath = `${path}/${resumeName}`;
  try {
    // If the local file exists, remove it to make space for the updated data
    if (fileSystem.existsSync(resumePath)) {
      await fileSystem.promises.rm(resumePath);
    }
  } catch (e) {
    throw Error("Error deleting old gist, remove manually to continue", e);
  }

  // Fetch the latest data from the remote gist
  const gist = await fetch(PUBLIC_GIST_URL)
    .then((r) => r.json())
    .catch((e) => console.error("Error loading gist: ", e));

  try {
    // Write the fetched data to the local file
    // creates: /Users/nvegater/js-dev/home/public/resume.json with updated gist data.
    await fileSystem.promises.writeFile(
      pathNode.join(path, "resume.json"),
      JSON.stringify(gist),
    );
  } catch (e) {
    console.error("Error updating file: ", e);
  }
}

/**
 * Reads and prints the content of a local file
 * @param {string} path - The path of the file to read
 */
function printResume(path) {
  fileSystem.readFile(path, async (err, data) => {
    if (err) throw Error("Error reading file: ", err);

    const stringFromFile = Buffer.from(data).toString("utf8");
    if (stringFromFile == null) throw Error("Invalid file in string");

    let fileContent;
    try {
      // Parse the string content of the file to JSON
      fileContent = JSON.parse(stringFromFile);
    } catch (e) {
      throw Error("Cant parse string");
    }

    // Print the content of the file to the console
    console.log(fileContent);
  });
}

/**
 * Main function to update the local file with the latest gist data, and print the updated content
 */
const outputUpdatedResume = async () => {
  // Determine the path to the public directory
  const path = pathNode.join(process.cwd(), "public");

  // Update the local file with the latest gist data
  await updateLocalFileFromGist(path);

  // Print the updated content of the local file
  printResume(`${path}/${resumeName}`);
};

// Execute the main function, and log any errors to the console
outputUpdatedResume().catch((e) => console.error("Error updating resume", e));

/*
Always need to replace the src image in the html because openresume forces you to use their own image storage service. I dont want to
<img
  class="media-object img-circle center-block"
  data-src="holder.js/100x100"
  alt="Nicolas Vega Terrazas"
  src="https://avatars.githubusercontent.com/u/45003959?u=e5ac59a4d8dcbf626a54c6a07acef263b51f252f&v=4"
  itemprop="image"

  />

* */
