import fs from "fs";
import { NextApiRequest, NextApiResponse } from "next";
export default async function api(req: NextApiRequest, res: NextApiResponse) {
  res.setHeader("Content-Type", "text/html; charset=utf-8");
  res.write(fs.readFileSync("/resume.html", "utf-8"));
  res.end();
}
