import React, { FC } from "react";
import neonStyles from "../../globals/neon.module.css";
import styles from "./ArticlesLayout.module.css";
import { ArticleCard } from "./ArticleCard";
import { type Post } from "content-collections";

interface ArticlesLayoutProps {
  posts: Post[];
  sectionTitle: string;
}

export const ArticlesLayout: FC<ArticlesLayoutProps> = ({
  posts,
  sectionTitle,
}) => {
  return (
    <>
      <section className={styles.postsList}>
        {posts.map((postMeta) => (
          <ArticleCard key={postMeta.title} {...postMeta} />
        ))}
      </section>
    </>
  );
};
