import React, { FC } from "react";
import styles from "./ArticleCard.module.css";
import Link from "next/link";
import { type Post } from "content-collections";

type PostMeta = Pick<Post, "title" | "excerpt" | "tags" | "date" | "_meta">;
export const ArticleCard: FC<PostMeta> = ({
  title,
  excerpt,
  tags,
  date,
  _meta: { path: url },
}) => {
  return (
    <article key={title} className={styles.card}>
      <Link href={`/blog/${url}`} passHref legacyBehavior>
        <h1>{title}</h1>
      </Link>
      <p>{excerpt}</p>
      <div className={styles.tags}>
        {tags?.map((t) => (
          <Link key={t} href={`/tags/${t}`} className={styles.tag}>
            {t}
          </Link>
        ))}
      </div>
      <time>{new Intl.DateTimeFormat("en-DE").format(new Date(date))}</time>
    </article>
  );
};
