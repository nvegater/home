import Head from "next/head";
import styles from "../app/blog/[slug]/Slug.module.css";
import neonStyles from "../globals/neon.module.css";
import React from "react";

import { MDXContent } from "@content-collections/mdx/react";
import { type Post } from "content-collections";

export const PostMDX = ({ post }: { post: Post }) => {
  return (
    <>
      <Head>
        <title>{post.title}</title>
      </Head>
      <div className={styles.articleWrapper}>
        <div className={styles.articleHeader}>
          <time style={{marginTop: "20px"}}>
            {new Intl.DateTimeFormat("en-DE").format(new Date(post.date))}
          </time>
        </div>
        <article className={styles.article}>
          <MDXContent code={post.mdx} />
        </article>
      </div>
    </>
  );
};
