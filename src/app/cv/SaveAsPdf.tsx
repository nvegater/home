"use client";

import React, { useRef } from "react";
import neonStyles from "../../globals/neon.module.css";

export const SaveAsPdf = () => {
  const iframeRef = useRef<HTMLIFrameElement>(null);

  const handlePrint = () => {
    if (iframeRef.current && iframeRef.current.contentWindow) {
      iframeRef.current.contentWindow.print();
    }
  };

  return (
    <>
      <a
        className={neonStyles.titleSecond}
        style={{ cursor: "pointer", display: "flex" }}
        onClick={handlePrint}
      >
        Save as pdf
      </a>
      <iframe
        ref={iframeRef}
        src="/resume.html"
        title="Resume"
        style={{ display: "none" }} // Hide the iframe is just for print function
      />
    </>
  );
};
