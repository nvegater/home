import React from "react";
import neonStyles from "../../globals/neon.module.css";
import styles from "./CV.module.css";
import { SaveAsPdf } from "./SaveAsPdf";

const CvPage = () => {
  return (
    <>
      <div className={styles.messageContainer}>
        <SaveAsPdf />
        <iframe
          src="/resume.html"
          className={styles.resumeIframe}
          title="Resume display"
        />
      </div>
    </>
  );
};

export default CvPage;
