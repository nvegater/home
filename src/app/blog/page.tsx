import { allPosts, type Post } from "content-collections";

import neonStyles from "../../globals/neon.module.css";
import styles from "./Blog.module.css";

import { ArticleCard } from "../../components/Articles/ArticleCard";

const compareDatesDesc = (a: Post, b: Post): number => {
  return new Date(b.date).getTime() - new Date(a.date).getTime();
};

const BlogPage = () => {
  const posts = allPosts.sort(compareDatesDesc);
  return (
    <>
      <section className={styles.postsList}>
        {posts.map((postMeta) => (
          <ArticleCard key={postMeta.title} {...postMeta} />
        ))}
      </section>
    </>
  );
};

export default BlogPage;
