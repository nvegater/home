import React from "react";
import "highlight.js/styles/atom-one-dark.css";
import { allPosts } from "content-collections";

import { PostMDX } from "@/src/components/PostMDX";

interface PostPageProps {
  params: Promise<{
    slug: string;
  }>;
}

export const generateStaticParams = () => {
  return allPosts.map((post) => ({ slug: post._meta.path }));
};

export async function generateMetadata({ params }: PostPageProps) {
  const searchParams = await params
  const post = getPostBySlug({ slug: searchParams.slug });
  return {
    title: post.title,
  };
}

const PostPage = async ({ params }: PostPageProps) => {
  const searchParams = await params
  const post = getPostBySlug({ slug: searchParams.slug });
  return <PostMDX post={post} />;
};

export default PostPage;

const getPostBySlug = ({ slug }: { slug?: string }) => {
  if (!slug) {
    throw Error("No Slug available!");
  }

  const onePost = allPosts.find((post) => {
    return post._meta.path === slug;
  });

  if (!onePost) throw Error("This should return 404");

  return onePost;
};
