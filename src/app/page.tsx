import NextLink from "next/link";

import styles from "./AppPage.module.css";
import Greeting from "./Greeting";

const Home = () => {
  return (
    <>
      <div className={styles.grid}>
        <div className={styles.mediumGridItem2}>
          <NextLink href="/about">
            <h2>About</h2>
          </NextLink>
        </div>
        <div className={styles.largeGridItem}>
          <NextLink href="/cv">
            <h2>Nicolas Vega Terrazas</h2>
          </NextLink>
        </div>
        <div className={styles.mediumGridItem}>
          <NextLink href="/blog">
            <h2>Blog</h2>
          </NextLink>
        </div>
        <div className={styles.smallGridItem}>
          <NextLink href="/cv">
            <h2>CV</h2>
          </NextLink>
        </div>
        <div className={styles.wideGridItem}>
          <NextLink href="/about">
            <code>Software Developer</code>
          </NextLink>
        </div>
        <div className={styles.tallGridItem}>
          <NextLink href="https://en.wikipedia.org/wiki/Berlin">
            <h2 className={styles.textInsideGridItem}>&#128205;BER</h2>
          </NextLink>
        </div>
      </div>
    </>
  );
};

export default Home;
