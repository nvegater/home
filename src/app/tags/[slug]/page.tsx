import React from "react";
import Head from "next/head";
import { ArticlesLayout } from "@/src/components/Articles/ArticlesLayout";
import { allPosts, type Post } from "content-collections";

function notEmpty<TValue>(value: TValue | null | undefined): value is TValue {
  return value !== null && value !== undefined;
}

export const generateStaticParams = () => {
  const tags = allPosts
    .map((p) => p.tags)
    .filter(notEmpty) // get only not Nullish Tags (defined)
    .flat();

  const uniqueTags = new Set(tags);

  return Array.from(uniqueTags).map((tag) => ({
    slug: tag,
  }));
};

const getPostsBySlug = ({ slug }: { slug?: string }): Post[] => {
  if (!slug) {
    throw Error("Invalid slug");
  }
  return allPosts.filter((post) => post.tags?.includes(slug));
};

// @ts-ignore TODO: types are not available https://github.com/vercel/next.js/issues/42840#issuecomment-1436076489
const TagsPage = ({ params }) => {
  const posts = getPostsBySlug({ slug: params?.slug });
  return (
    <>
      <Head>
        <title>Tag: {params?.slug}</title>
      </Head>
      <ArticlesLayout posts={posts} sectionTitle={`Tag: ${params?.slug}`} />
    </>
  );
};

export default TagsPage;
