import React, { FC } from "react";
import neonStyles from "../../globals/neon.module.css";
import styles from "./About.module.css";
import NextLink from "next/link";

interface AboutProps {}

const AboutPage: FC<AboutProps> = ({}) => {
  return (
    <>
      <article className={styles.about}>
        <p>
          To see information about myself please checkout my{" "}
          <NextLink href="/cv">CV</NextLink>
        </p>
        <p>
          The app is built with NextJs, React and Typescript deployed in Vercel, dead simple. Some more details:
        </p>
        <ul>
          <li>
            The stylesheet from <a href="https://edwardtufte.github.io/tufte-css/"> Tufte CSS</a> based on
            the ideas of Edward Tufte on how to style articles.
          </li>
          <li>
            <a href="src/app/about">CSS Modules</a>: Included in NextJs. Makes it very easily to create scalable
            and reusable styles
          </li>
          <li>
            <a href="https://www.content-collections.dev/">
              Content collections
            </a>:
            Transforms content into type-safe data collections without manual
            data fetching and parsing. Simply import content with built-in
            validation and preprocessing.
          </li>
          <li>
            <a href="https://www.youtube.com/watch?v=plRcoRqLriw">Mosaic </a>and{" "}
            {}
            <a href="https://www.youtube.com/watch?v=6xNcXwC6ikQ">
              neon title
            </a>{" "}
            by Kevin Powell
          </li>
          <li>
            <a href="https://www.youtube.com/watch?v=J_0SBJMxmcw">
              Video on making a blog
            </a>{" "}
            and transforming markdown with plugins using{" "}
            <a href="https://github.com/remarkjs/remark">Remark</a>
          </li>{" "}
          and <a href="https://github.com/rehypejs/rehype">rehype</a>
          <li>
            <a href="src/app/about">JSON resume</a>: an attempt to create CVs
            with open source standards.
          </li>
        </ul>
      </article>
    </>
  );
};

export default AboutPage;
