"use client";

import { FC, useEffect, useState } from "react";
import neonStyles from "../globals/neon.module.css";

const greeting = ["Hello", "Hola", "Hallo"];

const Greeting: FC = () => {
  const [index, setIndex] = useState(0);

  useEffect(() => {
    const updateState = () => {
      setIndex((prevIndex) => {
        if (prevIndex === greeting.length - 1) {
          return 0;
        }
        return prevIndex + 1;
      });
    };

    const interval = setInterval(updateState, 2000);

    return () => {
      clearInterval(interval);
    };
  }, []);

  return <h1 className={neonStyles.title}>{greeting[index]}</h1>;
};

export default Greeting;
